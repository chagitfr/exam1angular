import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { UsersComponent } from './src/app/users/users.component';
import { ProductComponent } from './src/app/product/product.component';
import { NotFoundComponent } from './src/app/not-found/not-found.component';
import { NavigationComponent } from './src/app/navigation/navigation.component';

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    ProductComponent,
    NotFoundComponent,
    NavigationComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
